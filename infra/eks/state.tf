terraform {

  backend "s3" {
    region         = "eu-central-1"
    bucket         = "silvanovich-eks"
    key            = "eks.tfstate"
    profile        = "default"
    role_arn       = ""
    encrypt        = "true"
  }
}