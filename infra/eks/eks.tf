data "aws_ami" "eks-worker" {
  owners      = ["602401143452"] # Amazon EKS AMI Account ID
  most_recent = true
  filter {
    name   = "name"
    values = ["amazon-eks-node-1.11-v*"]
  }
}

module "eks" {

  source = "../modules/terraform-aws-eks/"

  # -------------
  # Control Plane
  # -------------
  subnet_ids       = data.terraform_remote_state.vpc.outputs.public_subnet_ids
  eks_cluster_name = var.cluster_name
  eks_namespace    = var.eks_namespace
  vpc_id           = data.terraform_remote_state.vpc.outputs.vpc_id
  management_cidr  = var.cidr_block
  region           = var.region
  # -------------
  # Workers
  # -------------
  eks_worker_ami              = data.aws_ami.eks-worker.id
  eks_worker_instance_type    = var.worker_type
  eks_worker_desired_capacity = var.worker_desired_capacity
  eks_worker_min_size         = var.worker_min_size
  eks_worker_max_size         = var.worker_max_size


  eks_worker_ssh_key_name = var.key_name
  eks_worker_subnet_ids   = data.terraform_remote_state.vpc.outputs.private_subnet_ids
  #  eks_worker_on_demand_base_capacity                  = "1"
  #  eks_worker_on_demand_percentage_above_base_capacity = "100"

  eks_worker_group_name = var.worker_name
}
# -----------------------
# Echo out configs
# -----------------------


resource "local_file" "kubeconfig" {
  filename = "/home/aliaksei.silvanovich/.kube/eks-cluster"
  content  = module.eks.kubeconfig
}

resource "local_file" "config-map-aws-auth" {
  filename = "config-map-aws-auth.yaml"
  content  = module.eks.config-map-aws-auth
}

resource "local_file" "namespace" {
  filename = "namespace.yaml"
  content  = module.eks.namespace
}

resource "null_resource" "execute_kube_config" {
  provisioner "local-exec" {
    command = "export KUBECONFIG=~/.kube/eks-cluster"
  }
}

resource "null_resource" "execute_configmap" {
  provisioner "local-exec" {
    command = "kubectl apply -f config-map-aws-auth.yaml"

  }
}

resource "null_resource" "execute_namespace" {
  provisioner "local-exec" {
    command = "kubectl apply -f namespace.yaml"
  }
}

output "kubeconfig" { value = module.eks.kubeconfig }

output "config-map-aws-auth" { value = module.eks.config-map-aws-auth }

output "namespace" { value = module.eks.namespace }
