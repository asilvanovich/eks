data "terraform_remote_state" "vpc" {
  backend = "s3"

  config = {
    region         = "eu-central-1"
    bucket         = "silvanovich-eks"
    key            = "vpc.tfstate"
    profile        = "default"
    role_arn       = ""
    encrypt        = "true"
  }
}


