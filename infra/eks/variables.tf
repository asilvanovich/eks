variable "region" {
  type        = string
  description = "AWS Region"
}

variable "profile" {
  type        = string
  description = "AWS profile to use"
}

variable "namespace" {
  type        = string
  description = "Resource namespace"
}

variable "cluster_name" {
  type        = string
  description = "EKS cluster name"
}

variable "vpc_name" {
  type        = string
  description = "VPC name"
}

variable "worker_name" {
  type        = string
  description = "EKS worker nodes name"
}

variable "cidr_block" {
  type        = string
  description = "A range of IP addresses to use in VPC"
}

variable "k8s_version" {
  type        = string
  description = "K8s version"
}

#variable "asg_min_size" {
#  type        = number
#  description = "Minimum number of instances in ASG"
#}

#variable "asg_max_size" {
#  type        = number
#  description = "Maximum number of instances in ASG"
#}

variable "worker_type" {
  type        = string
  description = "Worker instance type"
}

variable "key_name" {
  type        = string
  description = "Default ssh key to launch hosts with"
}

variable "worker_desired_capacity" {
  description = "Desired number of EKS Worker nodes"
  type        = string
  default     = "1"
}
variable "worker_min_size" {
  description = "Minimum number of EKS Worker nodes"
  type        = string
  default     = "1"
}
variable "worker_max_size" {
  description = "Maximum number of EKS Worker nodes"
  type        = string
  default     = "3"
}

variable "eks_namespace" {
  description = "Namespace to create"
}
