module "vpc" {
  source = "../modules/terraform-aws-vpc"

  namespace  = var.namespace
  name       = var.vpc_name
  cidr_block = var.cidr_block

}