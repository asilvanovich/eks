data "aws_availability_zones" "available" {
}

locals {
  availability_zones = slice(data.aws_availability_zones.available.names, 0, 2)
}

module "subnets" {
  source = "../modules/terraform-aws-dynamic-subnets"

  namespace            = var.namespace
  name                 = var.vpc_name
  availability_zones   = local.availability_zones
  vpc_id               = module.vpc.vpc_id
  igw_id               = module.vpc.igw_id
  cidr_block           = var.cidr_block
  nat_gateway_enabled  = "true"

}

